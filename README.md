commerce_custom_checkout_pages_paths
=============

Drupal Commerce Custom Checkout pages paths.

This module allow users to create a custom paths
for Drupal commerce checkout pages.

Requirements:
 <b>Drupal Commerce.</b>

Installation:
 - install module
 - go to admin/commerce/config/commerce_ccpp page 
   and set a paths for checkout pages which you want
