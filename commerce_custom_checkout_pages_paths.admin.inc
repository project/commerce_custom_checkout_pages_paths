<?php
/**
 * @file
 * Drupal Commerce Custom Checkout pages paths admin page form.
 */

/**
 * Settings form for Commerce checkout pages custom paths.
 *
 * @ingroup forms
 * @see system_settings_form()
 */
function commerce_custom_checkout_pages_paths_settings_form($form, &$form_state) {
  $form = array();
  $form['ccpp_settings'] = array(
    '#type'           => 'fieldset',
    '#title'          => t('Commerce checkout pages paths configuration.'),
    '#description'    => t('Set your custom paths for a checkout pages like: shop/step1'),
  );

  foreach (_commerce_custom_checkout_pages_paths_get_checkout_pages_list() as $name => $data) {
    $title = t($data['title']) . ' <i>(machine_name: ' . t($name) . ')</i>';
    $value = variable_get('commerce_custom_checkout_pages_paths_' . $name, FALSE);
    $form['ccpp_settings']['commerce_custom_checkout_pages_paths_' . $name] = array(
      '#type'           => 'textfield',
      '#title'          => $title,
      '#default_value'  => $value,
      '#description'    => $data['description'],
    );
  }
  return system_settings_form($form);
}
