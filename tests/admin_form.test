<?php
/**
 * @file
 * Test for admin form settings.
 */

/**
 * Class CommerceCustomCheckoutPagesPaths.
 *
 * Basic tests for admin settings page.
 */
class CommerceCustomCheckoutPagesPaths extends DrupalWebTestCase {
  /**
   * Protected property of privileged user.
   *
   * @var object
   *   Object of privileged user.
   */
  protected $privilegedUser;

  /**
   * Setting a test info.
   *
   * @return array
   *   An array with an info data.
   */
  public static function getInfo() {
    return array(
      'name'        => 'Admin page test',
      'description' => 'Check is variables, which been set in admin page is saved correctly.',
      'group'       => 'Commerce CCPP',
    );
  }

  /**
   * Required setup processes before run a tests.
   */
  public function setUp() {
    // Load required modules.
    parent::setUp('commerce_custom_checkout_pages_paths');
    // Create permissions.
    $permissions = array('administer site configuration');
    // Create user with previously created permissions.
    $this->privilegedUser = $this->drupalCreateUser($permissions);
    // Login a privileged user.
    $this->drupalLogin($this->privilegedUser);
  }

  /**
   * Test setting/getting admin config page data.
   */
  public function testAdminVariables() {
    $name = 'cc_pp_checkout';
    $data = $this->randomString();
    $edit = array(
      $name => $data,
    );
    // Set a custom value to commerce checkout page "Checkout" path.
    $this->drupalPost('admin/commerce/config/commerce_ccpp', $edit, t('Save configuration'));
    // Check if options are saved.
    $this->assertText('The configuration options have been saved.', 'The configuration options have been saved.');
    $var = variable_get($name, FALSE);
    if ($var != FALSE && $var == $data) {
      $var = TRUE;
    }
    else {
      $var = FALSE;
    }
    // Check if a random path which we are set before are equal to
    // variable_get() value.
    $this->assertTrue($var, 'Variable has been set correctly');
  }
}
